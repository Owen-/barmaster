bl_info = {
	"name": "Fast Pie Menu",
	"category": "Object",
}

#retain selection would be nice to have, but without forcing vertex mode for selection
#would require redesign
#want to remove official pie menu addon dependency
#actual workflow would need to be though about but its a good start

import bpy, bmesh
from bpy.types import Menu

##########################################################
# MENU DECLARATIONS
##########################################################
	
class FastObjectMenu(Menu):
	bl_label = "Fast Menu"  						# label is displayed at the center of the pie menu.
	bl_idname = "object.fast_menu"

	def draw(self, context):
		layout = self.layout
		pie = layout.menu_pie()
		pie.operator("wm.call_menu_pie", text="Pivot", icon='PLUS').name = "PivotHeightMenu"
		pie.operator("wm.call_menu_pie", text="Shade", icon='PLUS').name = "VIEW3D_PIE_shade"
		pie.operator("wm.call_menu_pie", text="Gizmo", icon='PLUS').name = "VIEW3D_PIE_manipulator"
		pie.operator("wm.call_menu_pie", text="Snap", icon='PLUS').name = "VIEW3D_PIE_snap"
		
class FastMeshMenu(Menu):
	bl_label = "Selection Mode"  						# label is displayed at the center of the pie menu.
	bl_idname = "mesh.fast_menu"

	def draw(self, context):
		layout = self.layout
		pie = layout.menu_pie()
		pie.operator_enum("mesh.select_mode", "type")
		pie.operator("wm.call_menu_pie", text="Fast Menu", icon='PLUS').name = "object.fast_menu"
		
class PivotHeightMenu(Menu):
	bl_label = "Pivot/Height"   						# label is displayed at the center of the pie menu.

	def draw(self, context):
		layout = self.layout
		pie = layout.menu_pie()
		pie.operator ("object.set_unit_dimensions")#1
		pie.operator ("object.set_pivot_bottom_center")#2
		pie.operator ("wm.call_menu_pie", text="Pivot+", icon='PLUS').name = "VIEW3D_PIE_pivot"#3
		pie.operator ("object.set_unit_height_and_pivot") #4
		pie.operator ("object.set_unit_height")#5
		pie.operator ("object.set_pivot_base")#6
		
		
		


##########################################################
# OPERATOR DECLARATIONS
##########################################################

class SetUnitDimensions(bpy.types.Operator):
	"""Sets dimensions in all axis to 1 unit"""
	bl_idname = "object.set_unit_dimensions"
	bl_label = "(1,1,1) Dims"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		bpy.ops.object.mode_set(mode = 'OBJECT')
		set_unit_dimensions()

		return {'FINISHED'}

class SetUnitHeightAndPivot(bpy.types.Operator):
	"""Sets height to 1 and places pivot at base"""
	bl_idname = "object.set_unit_height_and_pivot"
	bl_label = "Unit Height & Base"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		bpy.ops.object.mode_set(mode = 'OBJECT')
		set_unit_height()
		set_pivot_to_base()
		bpy.ops.object.location_clear()
		return {'FINISHED'}

class SetUnitHeight(bpy.types.Operator):
	"""Sets height to 1 unit while keeping scale ratio"""
	bl_idname = "object.set_unit_height"
	bl_label = "Unit Height"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		bpy.ops.object.mode_set(mode = 'OBJECT')	
		set_unit_height()
		return {'FINISHED'}
	
class SetPivotToBase(bpy.types.Operator):
	"""Sets Pivot to the base (ignores rest of mesh)"""
	bl_idname = "object.set_pivot_base"
	bl_label = "Pivot Base"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		bpy.ops.object.mode_set(mode = 'OBJECT')
		set_pivot_to_base()
		return {'FINISHED'}

class SetPivotToBottomCenter(bpy.types.Operator):
	"""Sets Pivot to the bottom of mesh (averages all verts in other axis)"""
	bl_idname = "object.set_pivot_bottom_center"
	bl_label = "Pivot Bottom"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		bpy.ops.object.mode_set(mode = 'OBJECT')
		set_pivot_to_bottom_center()
		return {'FINISHED'}
	

				
##########################################################
# FUNCTION DECLARATIONS
##########################################################

def set_unit_dimensions():
	o  = bpy.context.object 
	oop = bpy.ops.object
	
	bpy.context.object.dimensions = 1,1,1
	oop.transform_apply(location=False, rotation=True, scale=True)
	oop.location_clear()


def set_pivot_to_base():
	clear_selection()
	select_base()
	origin_to_selection()
	clear_selection()
		
def set_unit_height():
	o  = bpy.context.object 
	oop = bpy.ops.object
	
	oop.transform_apply(location=False, rotation=True, scale=True)
	o.dimensions[2] = 1
	o.scale[1] = o.scale[2]
	o.scale[0] = o.scale[2]
	oop.transform_apply(location=False, rotation=True, scale=True)
	oop.location_clear()

def set_pivot_to_bottom_center():
	obj_bounds = selected_object_global_bounds() 
	
	bpy.ops.view3d.snap_cursor_to_active()
	bpy.context.space_data.cursor_location[2] = obj_bounds[2][0] # want to write this as XYZ notation eventually as a parameter
	bpy.ops.object.origin_set(type='ORIGIN_CURSOR')
	
def selected_object_global_bounds():
	return object_global_bounds(bpy.context.object)
			

def object_global_bounds(obj):
	mw = obj.matrix_world
	global_vcoords = [ mw * v.co for v in obj.data.vertices ]
	
	def min_bound(axis):
		return min( [ co[axis] for co in global_vcoords ] )
	def max_bound(axis):
		return max( [ co[axis] for co in global_vcoords ] )

	return [[min_bound(axis), max_bound(axis)] for axis in range(3)]
		
def origin_to_selection():
	bpy.ops.object.mode_set(mode = 'EDIT')
	bpy.ops.view3d.snap_cursor_to_selected()
	bpy.ops.object.mode_set(mode = 'OBJECT')
	bpy.ops.object.origin_set(type = "ORIGIN_CURSOR")
		
	
def clear_selection():
	bpy.context.tool_settings.mesh_select_mode = [True, False, False]
	bpy.ops.object.mode_set(mode = 'EDIT')
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode = 'OBJECT')
	
def selected_vert_indexs():
	vert_indexes = [v.index for v in bpy.context.object.data.vertices if v.select]
	return vert_indexes
	
def select_vert_indices(inds):
	for v in bpy.context.object.data.vertices:
		for i in inds:
			if v.index==i:
				v.select = True
				
def select_base():
	o = bpy.context.object
	mw = o.matrix_world
	
	for v in o.data.vertices:
		if (mw * v.co).z == selected_object_global_bounds()[2][0]:
			v.select = True

		
			
##########################################################
# REGISTERING FUNCTIONS
##########################################################
addon_keymaps = []

classes = (
	FastObjectMenu,
	FastMeshMenu,
	SetUnitDimensions,
	PivotHeightMenu,
	SetUnitHeightAndPivot,
	SetUnitHeight,
	SetPivotToBase,
	SetPivotToBottomCenter  
	)


def register():
	
	wm = bpy.context.window_manager
	
	for c in classes:
		bpy.utils.register_class(c)

	#for km in wm.keyconfigs.addon.keymaps:
	#	for kmi in km.keymap_items:
	#		if kmi.idname == 'wm.call_menu_pie':
	#			if kmi.properties.name == 'VIEW3D_PIE_snap':
	#				if kmi.type == 'TAB':
	#					km.keymap_items.remove(kmi)

	km = wm.keyconfigs.addon.keymaps.new(name ="Mesh")
	kmi = km.keymap_items.new("wm.call_menu_pie", "TAB", "PRESS", shift=True, ctrl=True).properties.name='mesh.fast_menu'
	addon_keymaps.append(km)
	
	km = wm.keyconfigs.addon.keymaps.new(name ="Object Mode")
	kmi = km.keymap_items.new("wm.call_menu_pie", "TAB", "PRESS", shift=True, ctrl=True).properties.name="object.fast_menu"
	addon_keymaps.append(km)
	
def unregister():
	for c in classes:
		bpy.utils.unregister_class(c)
		
	wm = bpy.context.window_manager

	if wm.keyconfigs.addon:
		for km in addon_keymaps:
			for kmi in km.keymap_items:
				km.keymap_items.remove(kmi)
			wm.keyconfigs.addon.keymaps.remove(km)

	addon_keymaps.clear()
	
if __name__ == '__main__':
    register()
