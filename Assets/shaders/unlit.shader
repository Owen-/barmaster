﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "SQUIDS/unlit"								// NAME OF SHADER
{
	Properties											// These properties are things that can actually be set in the inpector
	{
		_MainTex ("Texture", 2D) = "white" {}			// the only option is the texture you choose
	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100											// 'Level Of Detail'   a number you make up to roughly guage how graphically intensive your shader is (100 is low)

		Pass
		{
			CGPROGRAM									// between CGPROGRAM and ENDCG is where the actual shader language is
			#pragma vertex vert 						// the vertex shader will be called vert
			#pragma fragment frag 						// the fragment shader will be called frag
			
			#pragma multi_compile_fog   				// make fog work somehow?
			
			#include "UnityCG.cginc"



			struct appdata								// This defines the information that the vertex shader gets about each vertex 
			{
				float4 vertex : POSITION;				// the position of the vertex
				float2 uv     : TEXCOORD0;				// the UV texture coordinates of the vertex
			};

			struct v2f									// This defines the information that the fragment shader gets from the vertex shader about each fragment
			{
				float2 uv : TEXCOORD0;					// the texture coordinates
				UNITY_FOG_COORDS(1)						// THIS DOESNT HAVE A SEMI COLON, I THINK ITS INSERTING EXTRA VARIABLES THAT LET IT DO DISTANCE FOG
				float4 vertex : SV_POSITION;            // the vertex positions in screen space rather than world space
			};




			sampler2D _MainTex;							// the texture of the thing  (what is a sampler2D???) (this comes from the 'Properties' section at the top)
			float4    _MainTex_ST;						// x,y = scale of texture. z,w = offset of texture



				
			v2f vert (appdata v)								// THIS IS THE VERTEX SHADER
			{
				v2f o;											// initialise datapacket to send to fragment shader
				o.vertex = UnityObjectToClipPos(v.vertex);		// multiply vertex positions by MVP matrix to transform to screen coordinates
				o.uv     = TRANSFORM_TEX(v.uv, _MainTex);		// ??

				UNITY_TRANSFER_FOG(o,o.vertex);					// takes in screen space vertex coordinates and gives values to the SECRET EXTRA VARIABLES it added in earlier

				return o;										// send datapacket off to fragment shader
			}




			
			fixed4 frag (v2f i) : SV_Target						// THIS IS THE FRAGMENT SHADER
			{
				fixed4 col = tex2D(_MainTex, i.uv);				// get output colour by sampling the texture at the UV coordinates

				UNITY_APPLY_FOG(i.fogCoord, col);				// uses its secret extra variable (which turns out to be called fogCoord) to alter the output fragment colour (to make it foggy)

				return col;										// send final fragment colour off to become a pixel on the screen
			}

			ENDCG
		}
	}
}
