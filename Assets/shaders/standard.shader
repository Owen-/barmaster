﻿Shader "SQUIDS/standard" 
{
	Properties 
	{
		_Color      ( "Color",        Color      ) = (1,1,1,1)           // these are all inpector options (and their default values)
		_MainTex    ( "Albedo (RGB)", 2D         ) = "white" {}
		_Glossiness ( "Smoothness",   Range(0,1) ) = 0.5
		_Metallic   ( "Metallic",     Range(0,1) ) = 0.0
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		
		#pragma surface surf Standard fullforwardshadows        // Physically based Standard lighting model, and enable shadows on all light types

		#pragma target 3.0                                      // Use shader model 3.0 target, to get nicer looking lighting


		struct Input 
		{
			float2 uv_MainTex;
		};

		fixed4    _Color;			// input from inspector
		sampler2D _MainTex;
		half      _Glossiness;
		half      _Metallic;

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			fixed4 c     = tex2D (_MainTex, IN.uv_MainTex) * _Color;  	// Albedo comes from a texture tinted by color
			o.Albedo     = c.rgb;
			
			o.Metallic   = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha      = c.a;
		}

		ENDCG
	}

	FallBack "Diffuse"
}
