﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "SQUIDS/image effect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		//_invert  ("Invert", )
		[MaterialToggle] _invert("Invert", Int) = 0
	}

	SubShader
	{
		Cull Off ZWrite Off ZTest Always	// No culling or depth

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv     : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv     : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float _invert;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;										// no TRANSFORM_TEX with _MainTex?
				return o;
			}
			

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				
				if (_invert==1) col = 1 - col;						// just invert the colors. i mean, WHY THE FUCK WOULDNT I INVERT THEM??
				return col;
			}
			ENDCG
		}
	}
}
