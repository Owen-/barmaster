﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "SQUIDS/post effect"
{
	Properties
	{
		_MainTex   ("Main Texture",  2D) = "white" {}
		_NoiseTex  ("Noise Texture", 2D) = "white" {}
		_TintColour("Main Colour", COLOR) = (1,1,1,1)

		[MaterialToggle] _invert   ("Invert",    Int) = 0 // have to use int because bool is not supported
		[MaterialToggle] _greyscale("Greyscale", Int) = 0
		[MaterialToggle] _noise    ("Noise",     Int) = 0
		[MaterialToggle] _waves    ("Waves",     Int) = 0
		[MaterialToggle] _tint     ("Tint",      Int) = 0
	}

	SubShader
	{
		Cull Off ZWrite Off ZTest Always	// No culling or depth

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex  : POSITION;
				float2 uv      : TEXCOORD0;
				float2 noiseuv : TEXCOORD1;
			};

			struct v2f
			{
				float4 vertex  : POSITION;
				float2 uv      : TEXCOORD0;
				float2 noiseuv : TEXCOORD1;
			};

			sampler2D _MainTex;
			sampler2D _NoiseTex;
			float4    _MainTex_ST;
			float4    _NoiseTex_ST;
			fixed4    _TintColour;
			int _invert;
			int _greyscale;
			int _noise;
			int _waves;
			int _tint;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex  = UnityObjectToClipPos(v.vertex);
				o.uv      = TRANSFORM_TEX(v.uv, _MainTex);		  // simple macro that apples texture tiling/scaling
				o.noiseuv = TRANSFORM_TEX(v.noiseuv, _NoiseTex);		
				return o;
			}
			

			fixed4 frag (v2f i) : SV_Target
			{

				if (_waves==1)     i.uv += float2(0,0.1*cos(i.uv.x/0.2 + 0.5*_Time[1]));

				fixed4 col = tex2D(_MainTex, i.uv);
				
				if (_invert==1)    col = 1 - col;		
				if (_greyscale==1) col = 0.21*col.r + 0.72*col.g + 0.07*col.b;
				if (_tint==1)      col = col * _TintColour;

				//float r = 0.1*tex2D(_NoiseTex, float2(0.1,_Time[1]/20) ).x;

				//if (_noise==1)     col = tex2D(_NoiseTex, i.noiseuv+r);

				return col;
			}
			ENDCG
		}
	}
}
