using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CreateAssetMenu(menuName="Barmaster/LevelGroup")]
public class LevelGroup : ScriptableObject
{
	public List<Level> levels;
}
