using UnityEngine;
using UnityEditor;
using System.Collections;

[CreateAssetMenu(menuName="Barmaster/Flightpath")]
public class Flightpath : ScriptableObject
{
	public AnimationCurve x, y, z;    // defines path though space with respect to time
	public AnimationCurve a;          // angle, w.r.t. time

	private Vector3    Aposition, Bposition;
	private Quaternion Arotation, Brotation;

	private Transform B; 

	public void setTarget(Transform _A, Transform _B)
	{
		Aposition = _A.position; // fixed start point
		Arotation = _A.rotation;

		B = _B;                  // reference copy of B, so that target can move
	}

	public void setTarget(Transform _A, Vector3 _Bposition, Quaternion _Brotation)
	{
		Aposition = _A.position; // fixed start point
		Arotation = _A.rotation;

		Bposition = _Bposition;  // fixed end point
		Brotation = _Brotation;

		B = null;
	}

	public void setTarget(Vector3 _Aposition, Quaternion _Arotation, Transform _B)
	{
		Aposition = _Aposition;  // fixed start point
		Arotation = _Arotation;

		B = _B;                  // reference copy of B, so that target can move
	}

	public void setTarget(Vector3 _Aposition, Quaternion _Arotation, Vector3 _Bposition, Quaternion _Brotation)
	{
		Aposition = _Aposition;  // fixed start point
		Arotation = _Arotation;

		Bposition = _Bposition;  // fixed end point
		Brotation = _Brotation;

		B = null;
	}

	public void evaluate(Transform T, float t)
	{
		if (t<0 || t>1) Debug.Log("flightpath time out of range");

		if (B!=null) { Bposition = B.position; Brotation = B.rotation; }

		Vector3 P = new Vector3(x.Evaluate(t), y.Evaluate(t), z.Evaluate(t)); // calculate normalised progress vector

		T.position = Aposition + Vector3.Scale(Bposition-Aposition,P);
		T.rotation = Quaternion.Slerp(Arotation, Brotation, a.Evaluate(t)); 
	}		

	// add functionality to evaluate in reverse for return journey
}

