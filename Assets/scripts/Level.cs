using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CreateAssetMenu(menuName="Barmaster/Level")]
public class Level : ScriptableObject
{
	public List<DrinkType.DrinkTypeWithProbability> drinks; // drinkTypesWithProbabilities ?
}
