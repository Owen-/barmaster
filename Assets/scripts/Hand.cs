using UnityEngine;

public class Hand
{
	public delegate void Handler(Hand h);
	private Handler handler;

	public SectionMember member;
	public Section       section;
	public Drink         drink;
	public Transform     focus;

	private const float _deadzone = 0.1f;
	public static float deadzone { get { return _deadzone; } }

	private bool _isLeft;  // handedness
	public  bool isLeft { get { return _isLeft; } }

	private bool _A;      
	public  bool  A { get { return _A; } } // action button

	private float _x, _y, _r, _a, _T;  
	public  float x { get { return _x; } } // x and y joystick values
	public  float y { get { return _y; } }
	public  float r { get { return _r; } } // radius and angle of joystick
	public  float a { get { return _a; } }
	public  float T { get { return _T; } } // trigger value

	public Hand(bool b, Handler h, Transform _focus)   
	{ 
		_isLeft = b; 
		setState(h); 
		focus = _focus;
	}

	public void setState(Handler h) 
	{ 
		handler = h; 
	}

	public void act()                
	{ 
		getInput();
		handler(this); 
	}

	public void getInput()
	{
		if (_isLeft) 
		{ 
			_x = Input.GetAxis("LX");
			_y = Input.GetAxis("LY"); 
			_A = Input.GetButtonDown("left action");
			_T = Input.GetAxis("left trigger");
		}
		else      
		{ 
			_x = Input.GetAxis("RX"); 
			_y = Input.GetAxis("RY"); 
			_A = Input.GetButtonDown("right action");
			_T = Input.GetAxis("right trigger");
		}

		_a = Mathf.Atan2(_y,_x) * Mathf.Rad2Deg;
		_r = Mathf.Sqrt(_x*_x + _y*_y);
	}

	public void setSection(Section _section)
	{
		if (!System.Object.ReferenceEquals(_section,section))
		{
			if (section != null) section.unhighlight(); // need nullcheck or better?
			section = _section;
			section.highlight();
		}
	}

	public void setMember(SectionMember _member)
	{
		if (!System.Object.ReferenceEquals(_member,member))
		{
			if (member != null) member.unhighlight();
			member = _member;
			member.highlight();
		}
	}
}