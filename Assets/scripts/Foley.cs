using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Foley : MonoBehaviour
{
	private AudioSource source;

	public AudioClip pop,
	                 pour,
	                 whoosh,
	                 placeDown,
	                 pickUp;

	private void Awake() { source = GetComponent<AudioSource>(); }

	public void audioPop()       { source.clip = pop;       source.Play(); }
	public void audioPour()      { source.clip = pour;      source.Play(); }
	public void audioWhoosh()    { source.clip = whoosh;    source.Play(); }
	public void audioPlaceDown() { source.clip = placeDown; source.Play(); }
	public void audioPickUp()    { source.clip = pickUp;    source.Play(); }

	public void stop() { source.Stop(); }
}