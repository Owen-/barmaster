using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class OrderMaker : MonoBehaviour 
{
	public static Order generateDrinkOrder(Level level)
	{
		Order order = new Order();

		int r = Random.Range(1,6);

		for (int j=0; j<r; j++)  // change 3 for randomised number
		{
			float P = 0;
			foreach (var d in level.drinks) P += d.p;

			int n;   // index or chosen drink

			do    { n = Random.Range(0, level.drinks.Count); }   // picks random drink
			while ( Random.value > level.drinks[n].p/P );        // discard if coin flip fails

			// should we exit the loop if it goes round 100 times without finding a drink?

			order.drinks.Add(level.drinks[n].d);
		}

		order.t = Time.time;// + 5f;

		return order;
	}
}

