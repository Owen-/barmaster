using UnityEngine;
using System.Collections;


public class Tap : SectionMember
{
	public  Ingredient head;
	public  DrinkType type;
	public  Transform nozzle;
	public  Transform lever;

	public  ParticleSystem particles;
	private ParticleSystem.EmissionModule em;

	private const float pourSpeed = 30f;

	public AnimationCurve fluidToHeadRatio;

	protected override void Start()
	{
		base.Start();
		Overlord.instance.sectionManager.tapSection.addMember(this);
		// Debug.Log("tap added to tap section");	

		em = particles.emission;
		particles.startColor = ingredient.color;
	}

	public override void setUp(Hand hand)
	{
		base.setUp(hand);

		hand.drink = Instantiate(Overlord.instance.drinkPrefab, nozzle, false) as Drink;
		hand.drink.setType(type);
		hand.drink.summonGlass(type.glass,hand.focus,1f);
		hand.drink.pint_MakeCylinders(ingredient.material, head.material);
	}

	public override void selfManage(Hand hand)
	{
		lever.localRotation = Quaternion.Euler(-90-90*hand.T,0,0);
		hand.drink.transform.localRotation = Quaternion.Euler(0,0,45*hand.x);

		if (hand.T>0.1) 
		{
			em.enabled = true;
			
			float V_fluid =    fluidToHeadRatio.Evaluate(hand.T)  * pourSpeed * Time.deltaTime,
			      V_head  = (1-fluidToHeadRatio.Evaluate(hand.T)) * pourSpeed * Time.deltaTime;

			hand.drink.addIngredient(ingredient, V_fluid);
			hand.drink.addIngredient(head, V_head);

			hand.drink.pint_UpdateCylinders(ingredient,head);
		}
		else em.enabled = false;

		if (hand.A)	shutDown();
	}

	public override void shutDown()
	{
		base.shutDown();
		em.enabled = false;

		// give glass/drink model object to orderManager to keep on front
		// and make a new empty glass?
	}
}
