using UnityEngine;
using System.Collections;


public class Spirit : SectionMember
{
	protected override void Start()
	{
		base.Start();
		Overlord.instance.sectionManager.spiritSection.addMember(this);
		// Debug.Log("spirit added to spirit section");	
	}
}