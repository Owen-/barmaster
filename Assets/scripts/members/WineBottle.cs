using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Foley))]
public class WineBottle : SectionMember
{
	public DrinkType type;

	private bool isPouring = false;
	private float angle;

	private const float maxPourSpeed = 100,
	                    minPourSpeed = 20;

	private const float particleMinSize  = 0.005f,
	                    particleMaxSize  = 0.020f;

	public  AnimationCurve particleSpeedCurve;       // determines particle speed based on normalised bottle angle
	public  AnimationCurve holdingStrengthCurve;     // determines how far the bottle gets towards the desired angle

	public  ParticleSystem particles;
	private ParticleSystem.EmissionModule em;
	public  float pouringThreshold;

	private Transform target;	
	public  Flightpath wineFlightpath;//, glassFlightpath;
	private Flightpath wineFP;//, glassFP;

	protected override void Start()
	{
		base.Start();
		Overlord.instance.sectionManager.wineSection.addMember(this);
		// Debug.Log("bottle added to wine section");	

		em = particles.emission;
		particles.startColor = ingredient.color;

		wineFP  = Object.Instantiate(wineFlightpath)  as Flightpath;
	}

	private void FixedUpdate()
	{
		state = animator.GetCurrentAnimatorStateInfo(0); // cache info of current animation state

		if (state.IsName("base.flying to centre") || state.IsName("base.flying to shelf"))
		{
			wineFP.evaluate(transform, state.normalizedTime);
		}
	}

	public override void setUp(Hand hand)
	{
		base.setUp(hand);

		target = (new GameObject("wine target")).transform;
		target.SetParent(hand.focus,false);

		hand.drink = Instantiate(Overlord.instance.drinkPrefab, hand.focus, false) as Drink;
		hand.drink.setType(type);
		hand.drink.summonGlass(type.glass,hand.focus, 1);

		wineFP.setTarget(transform, target); 
		Debug.Log("wine setUp");
	}

	public override void selfManage(Hand hand)
	{
		calculatePouringAngle(hand);

		if (state.IsName("base.flying to centre"))
		{
			target.rotation = Quaternion.Euler(0,0,angle+90);
		}
		if (state.IsName("base.pouring"))
		{
			pouringEffects(hand);
			transform.rotation = Quaternion.Euler(0,0,angle+90);
		}
	}

	public override void shutDown()
	{
		base.shutDown();

		Destroy(target.gameObject);
		wineFP.setTarget(transform, originalPosition, originalRotation);
		em.enabled = false;
	}

	private void calculatePouringAngle(Hand hand)
	{
		float a0 = hand.isLeft? -135 : -45;

		if (hand.r < Hand.deadzone) angle = a0;
		else                        angle = a0 + (hand.a-a0)*holdingStrengthCurve.Evaluate(hand.r);
        
        angle = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0,0,angle+90), Time.deltaTime*6f).eulerAngles.z-90;

		if(angle>180) angle = angle - 360;
	}

	private void pouringEffects(Hand hand)
	{
		if ( angle>pouringThreshold && !isPouring )             // turn on audio/particles
		{
			em.enabled = true; 
			foley.audioPour();
			isPouring = true;
		}
		else if ( angle<pouringThreshold && isPouring )         // turn off audio/particles
		{
			em.enabled = false; 
			foley.stop();
			isPouring = false;
		}

		if (isPouring)
		{
			float s = (1f - Mathf.Abs(angle-90f)/90f);  // normalised pour angle
			float V = (minPourSpeed + (maxPourSpeed-minPourSpeed)*s)*Time.deltaTime; // volume poured this frame

			// Debug.Log("s = " + s + "   V = " + V);
			hand.drink.addIngredient(ingredient, V);

			particles.startSize  = particleMinSize + (particleMaxSize-particleMinSize)*s;  // equivalent calculation for particle size
			particles.startSpeed = particleSpeedCurve.Evaluate(s);
		}
	}
}