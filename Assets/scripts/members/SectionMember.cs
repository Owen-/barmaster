using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public abstract class SectionMember : MonoBehaviour 
{
	protected Vector3    originalPosition;
	protected Quaternion originalRotation;

	protected Animator animator;
	protected AnimatorStateInfo state;  // cached info of current animation state

	protected Foley foley;              // sound effect manager component

	public Ingredient ingredient;       // what ingredient does this correspond to

	protected virtual void Start()	
	{
		animator = GetComponent<Animator>();
		foley    = GetComponent<Foley>();
		originalPosition = transform.position;
		originalRotation = transform.rotation;
	}	 

	public void highlight ()  { animator.SetBool(Hash.highlighted, true); }
	public void unhighlight() { animator.SetBool(Hash.highlighted, false); }

	public void triggerAction() { animator.SetTrigger(Hash.action); }


	public virtual void setUp(Hand hand)
	{
		triggerAction();
		unhighlight();
	}

	public virtual void selfManage(Hand hand)
	{
		Debug.Log("you just called virtual parent function 'selfManage()' ");
		shutDown(); // shut down immediately if no override
	}

	public virtual void shutDown()
	{
		triggerAction();
		foley.stop();
	}
}

