using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Order // : MonoBehaviour 
{
	public List<DrinkType> drinks;

	public RectTransform bubble;

	public bool _isComplete = false;
	public bool isComplete { get { return _isComplete; } }

	public float t; // time the order appears

	public int count { get { return drinks.Count; } }

	public Order()
	{
		drinks = new List<DrinkType>();
	}

	public void setComplete(bool complete)
	{
		if (complete != _isComplete)
		{
			_isComplete = complete;
			Overlord.instance.uiManager.setOrderComplete(this);
		}
	}
}