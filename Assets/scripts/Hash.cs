using UnityEngine;

public static class Hash
{
	public static readonly int 
		action      = Animator.StringToHash("action"),
		highlighted = Animator.StringToHash("highlighted");
}