using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
 
public static class ListFunctions
{

	// public static bool test<T>(T a, T b) where T : IEquatable<T>
	// {
	// 	// return a==b;
	// 	return a.Equals(b);
	// }

	// // public static bool isSubset1<T>(this IList<T> smallSet, IList<T> bigSet) where T : class, IEquatable<T>
	// // {
	// // 	// return !smallSet.Except(bigSet).Any();

	// // 	return (smallSet.Where(s => !bigSet.Any(b => b == s))).Any();
	// // }

	// public static bool isSubset2<T>(this IList<T> smallSet, IList<T> bigSet) where T : int//IEquatable<T>
	// {
	// 	// return !smallSet.Except(bigSet).Any();

	// 	return (bigSet.Where(s => !smallSet.Any(b => b.Equals(s)))).Any();
	// }

	// public static bool isSubsetOf<T>(this IList<T> smallList, IList<T> bigList)
	// {
	// 	if (bigList==null || smallList==null) return false;
	// 	if (smallList.Count > bigList.Count)  return false;

	// 	var d = new Dictionary<T, int>();

	// 	foreach (T m in smallList)
	// 	{
	// 		if (d.ContainsKey(m)) d[m]++;
	// 		else                  d.Add(m, 1);
	// 	}

	// 	foreach (T m in bigList)
	// 	{
	// 		if (d.ContainsKey(m) && d[m]>0 ) 
	// 			d[m]--;
	// 	}

	// 	return d.Values.All(c => c == 0);
	// }

	// public static bool isSupersetOf<T>(this IList<T> bigList, IList<T> smallList)
	// {
	// 	return isSubsetOf<T>(smallList, bigList);
	// }


	// public static bool scrambledEquals<T>(this IList<T> bigList, IList<T> smallList)
	// {
	// 	if (bigList==null || smallList==null) return false; // maybe change to xor (^)
	// 	if (smallList.Count != bigList.Count) return false;

	// 	var d = new Dictionary<T, int>();

	// 	foreach (T m in bigList)
	// 	{
	// 		if (d.ContainsKey(m))   d[m]++;
	// 		else                    d.Add(m, 1);
	// 	}

	// 	foreach (T m in smallList)
	// 	{
	// 		if (d.ContainsKey(m))   d[m]--;
	// 		else                    return false;
	// 	}

	// 	return d.Values.All(c => c == 0);
	// }

	public static bool isSubsetOf<T2, T1>(this IList<T2> smallList, IList<T1> bigList) where T2 : IEquivalentable
	{
		if (bigList==null || smallList==null) return false;
		if (smallList.Count > bigList.Count)  return false;

		var d = new Dictionary<T2, int>();

		foreach (T2 m in smallList)
		{
			if (d.ContainsKey(m))   d[m]++;
			else                    d.Add(m, 1);
		}

		foreach (T1 n in bigList)
		foreach (T2 m in smallList)
		{
			if (m.isEquivalentTo<T1>(n))
			{
				if (d.ContainsKey(m) && d[m]>0 ) 
					d[m]--;

				break;
			}
		}

		return d.Values.All(c => c == 0);
	}

	// public static bool IsSupersetOf<T1, T2>(this IList<T1> bigList, IList<T2> smallList) where T2 : IEquivalentable
	// {
	// 	return isSubsetOf<T2,T1>(smallList, bigList);
	// }

}

public interface IEquivalentable
{
	bool isEquivalentTo<T> (T target);
}