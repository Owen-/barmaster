using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class OrderManager : MonoBehaviour 
{
	public  LevelGroup levelGroup;
	private Level _level;
	public  Level level { get { return _level; } }

	private List<Drink> madeDrinks;
	private List<Order> currentOrders;
	private List<Order> futureOrders;

	public Transform madeDrinksTarget;
	[HideInInspector] public Vector3 nextMadeDrinkPosition
		{ get { return madeDrinksTarget.position + new Vector3(-0.1f*madeDrinks.Count,0,0); } }

	private void Start()
	{
		madeDrinks    = new List<Drink>();
		currentOrders = new List<Order>();
		futureOrders  = new List<Order>();

		_level = levelGroup.levels[3-1];

		for (int i=0; i<2; i++)  // generate 2 drink orders
			futureOrders.Add( OrderMaker.generateDrinkOrder(_level) );

		// Debug.Log("number of future orders: " + futureOrders.Count);
	}

	private void Update()
	{
		if (futureOrders.Count>0 && futureOrders[0].t < Time.time)
		{
			currentOrders.Add(futureOrders[0]);  
			Overlord.instance.uiManager.displayDrinkOrder(futureOrders[0]);   
			futureOrders.RemoveAt(0);

			// Debug.Log(currentOrders.Count);
		}

		// spawnDrinksDebug();

		// if (futureOrders.Count <= 3)
		// {
		// 	Debug.Log("only 3 futureOrders remain. you should write code to generate more");
		// 	// make more futureOrders
		// }
	}

	private void checkOrderCompletion()
	{
		foreach ( Order order in currentOrders )
		{
			if (order.drinks.isSubsetOf<DrinkType,Drink>(madeDrinks))
				order.setComplete(true);
			else
				order.setComplete(false);
		}
	}

	public void sendOrder(Order order)
	{
		foreach( DrinkType od in order.drinks )          // removes the drinks from madeDrinks
		{
			for (int i=0; i<madeDrinks.Count; i++)
			{
				if (od.isEquivalentTo<Drink>(madeDrinks[i]))
				{
					madeDrinks.RemoveAt(i);
					break;
				}

				if (i==madeDrinks.Count-1) Debug.Log("ERROR: order being sent was not complete");
			}
		}
		
		currentOrders.Remove(order);

		Destroy(order.bubble.gameObject); // undraw the bubble
		checkOrderCompletion();
		Overlord.instance.uiManager.displayMadeDrinks(madeDrinks);
	}

	public void addMadeDrink(Hand hand)
	{
		hand.drink.transform.SetParent(madeDrinksTarget);
		// hand.drink.transform.localPosition = new Vector3(madeDrinks.Count*-0.1f,0f,0f);
		madeDrinks.Add(hand.drink);
		hand.drink = null;

		checkOrderCompletion();
		Overlord.instance.uiManager.displayMadeDrinks(madeDrinks);

		// PUT DRINK IN QUEUE ON FRONT BAR
	}

	// public void addMadeDrink(DrinkType type)
	// {
	// 	Drink drink = Instantiate(Overlord.instance.drinkPrefab);
	// 	drink.setType(type);

	// 	madeDrinks.Add(drink);
		
	// 	checkOrderCompletion();
	// 	Overlord.instance.uiManager.displayMadeDrinks(madeDrinks);
	// }

	// private void spawnDrinksDebug()
	// {
	// 	if (Input.GetKeyDown("r")) addMadeDrink(Resources.Load("order drinks/red wine")   as DrinkType);
	// 	if (Input.GetKeyDown("w")) addMadeDrink(Resources.Load("order drinks/white wine") as DrinkType);
	// 	if (Input.GetKeyDown("l")) addMadeDrink(Resources.Load("order drinks/lager")      as DrinkType);
	// 	if (Input.GetKeyDown("v")) addMadeDrink(Resources.Load("order drinks/vodka")      as DrinkType);

	// 	if (Input.GetKeyDown("p")) { checkOrderCompletion(); Debug.Log("CHECK"); }
	// }
}