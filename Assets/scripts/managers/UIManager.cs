using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;



public class UIManager: MonoBehaviour 
{
	public RectTransform orderContainer;    // container rect which stores order bubbles (currently the canvas)
	public RectTransform bubblePrefab;      // order bubble UI prefab
	public RectTransform drinkSpritePrefab; // drink image  UI prefab

	public RectTransform madeDrinksBubble;  // reference to the specific bubble where madeDrinks go

	public Color bubbleComplete, 
	             bubbleIncomplete;

	public void displayDrinkOrder(Order order)
	{
		order.bubble = Instantiate(bubblePrefab) as RectTransform;
		order.bubble.SetParent(orderContainer);
		order.bubble.GetComponent<ClickCompletion>().order = order;
		// Debug.Log("bubble");

		order.bubble.offsetMin = new Vector2( 50, order.bubble.offsetMin.y ); // ASK OWEN LATER

		for (int i=0; i<order.count; i++) // draw textures into bubble
		{
			RectTransform drinkSprite = Instantiate(drinkSpritePrefab) as RectTransform;
			drinkSprite.SetParent(order.bubble);

			drinkSprite.GetComponent<Image>().sprite = order.drinks[i].sprite;
		}
	}

	public void setOrderComplete(Order order)
	{
		if (order.isComplete) order.bubble.GetComponent<Image>().color = bubbleComplete;
		else                  order.bubble.GetComponent<Image>().color = bubbleIncomplete;
	}

	public void displayMadeDrinks(List<Drink> madeDrinks)
	{
		var children = new List<GameObject>();
		foreach (Transform child in madeDrinksBubble.transform) children.Add(child.gameObject);
		children.ForEach(child => Destroy(child));

		foreach (Drink d in madeDrinks)
		{
			RectTransform drinkSprite = Instantiate(drinkSpritePrefab) as RectTransform;
			drinkSprite.SetParent(madeDrinksBubble);

			if (d.type != null)
				drinkSprite.GetComponent<Image>().sprite = d.type.sprite;
		}
	}

}
