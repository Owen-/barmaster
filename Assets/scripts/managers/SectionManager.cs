using UnityEngine;
using System.Collections;

public class SectionManager : MonoBehaviour
{
	public TapSection    tapSection;
	public WineSection   wineSection;     // references to sections
	public SpiritSection spiritSection;

	public void choosingSection(Hand hand)
	{
		if (hand.r > Hand.deadzone)
		{
			if      (hand.a>=0   && hand.a<90 ) { hand.setSection(wineSection);   }
			else if (hand.a>=90  && hand.a<180) { hand.setSection(spiritSection); }
			else if (hand.a>=180 && hand.a<270) { hand.setSection(tapSection);    }
			// else if (hand.a>=270 && hand.a<360) { hand.setSection(fridgeSection); }

			if (hand.A)
			{
				hand.setState(hand.section.choosingMember);
				Debug.Log("SECTION chosen with " + (hand.isLeft? "LEFT" : "RIGHT") + " hand.");
			}
		}
	}
}








