using UnityEngine;
using System.Collections;

[RequireComponent(typeof(OrderMaker))]
[RequireComponent(typeof(OrderManager))]
[RequireComponent(typeof(SectionManager))]
[RequireComponent(typeof(UIManager))]
public class Overlord : MonoBehaviour 
{
	public static Overlord instance;

	public Transform leftFocus, rightFocus;

	public Drink drinkPrefab,
	             mixedDrinkPrefab;

	// make these static
	
	[HideInInspector] public OrderMaker     orderMaker;
	[HideInInspector] public OrderManager   orderManager;
	[HideInInspector] public SectionManager sectionManager;
	[HideInInspector] public UIManager      uiManager;

	private Hand left, right;     // hand objects

	protected virtual void Awake () 
	{
		if (instance == null)
		{
			DontDestroyOnLoad(gameObject);
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}

		orderMaker     = GetComponent<OrderMaker>();
		orderManager   = GetComponent<OrderManager>();
		sectionManager = GetComponent<SectionManager>();
		uiManager      = GetComponent<UIManager>();
	}

	private void Start()
	{
		left  = new Hand(true,  sectionManager.choosingSection, leftFocus);
		right = new Hand(false, sectionManager.choosingSection, rightFocus);
	}

	private void Update()
	{
		left.act();  // make hands do work
		right.act();
	}
	
}
