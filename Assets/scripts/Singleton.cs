using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T instance;

	public static T i
	{
		get 
		{ 
			if (instance == null) Debug.Log("ERROR: no instance of referenced singleton exists.");
			return instance; 
		}
	}

	protected virtual void Awake () 
	{
		if (instance == null)
		{
			DontDestroyOnLoad(gameObject);
			instance = this as T;
		}
		else
		{
			Destroy(gameObject);
		}
	}	
}
