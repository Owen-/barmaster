using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(Ingredient.IngredientWithQuantity))]
public class IngredientWithQuantityDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position, label, property);
	
			int indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 1;  

			Rect qRect = new Rect (position.x,    position.y, 50,                position.height);
			Rect iRect = new Rect (position.x+50, position.y, position.width-50, position.height);
			
			EditorGUI.PropertyField(qRect, property.FindPropertyRelative("q"), GUIContent.none);
			EditorGUI.PropertyField(iRect, property.FindPropertyRelative("i"), GUIContent.none);

			EditorGUI.indentLevel = indent;
		
		EditorGUI.EndProperty ();
	}
}