using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(DrinkType.DrinkTypeWithProbability))]
public class DrinkTypeWithProbibilityDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		// Using BeginProperty / EndProperty on the parent property means that
		// prefab override logic works on the entire property.
		EditorGUI.BeginProperty(position, label, property);

		// Draw the text label just to the left of the thing
		// position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		int indent = EditorGUI.indentLevel; // save real indent-level value for later
		EditorGUI.indentLevel = 1;          // set indent level to 1

		// Calculate rects
		Rect pRect     = new Rect (position.x,    position.y, 50,                position.height);
		Rect drinkRect = new Rect (position.x+50, position.y, position.width-50, position.height);
		
		// Draw fields - passs GUIContent.none to each so they are drawn without labels
		EditorGUI.PropertyField(pRect,     property.FindPropertyRelative("p"), GUIContent.none);
		EditorGUI.PropertyField(drinkRect, property.FindPropertyRelative("d"), GUIContent.none);

		EditorGUI.indentLevel = indent;    // Set indent-level back to the saved value
		
		EditorGUI.EndProperty ();
	}
}