﻿using UnityEngine;
using System.Collections;


public static class PivotCalculation 
{
	// Move verts in opposite direction = move pivot in right direction.
	public static void SetOrigin(this MeshFilter MF, Vector3 T) 							
	{ 
		MF.TransformTRS(T*-1f, Quaternion.identity, Vector3.one);
		MF.transform.position = T;
	}
	// Convets the various aliases for TransformTRS into the main function
	public static void Rotate(this MeshFilter MF, Vector3 R) 								{ MF.TransformTRS(Vector3.zero, Quaternion.Euler(R), Vector3.one); }
	public static void Rotate(this MeshFilter MF, Quaternion Q) 							{ MF.TransformTRS(Vector3.zero, Q, Vector3.one); }
	public static void Scale(this MeshFilter MF, Vector3 S)								{ MF.TransformTRS(Vector3.zero, Quaternion.identity, S); }
	public static void TransformTRS(this MeshFilter MF, Vector3 T, Vector3 R, Vector3 S)	{ MF.TransformTRS(T, Quaternion.Euler(R), S); }

	public static void TransformTRS(this MeshFilter MF, Vector3 T, Quaternion Q, Vector3 S)
	{
		Vector3[] Vs = new Vector3[MF.mesh.vertices.Length];
		for (int i = 0; i<MF.mesh.vertices.Length; i++)
			Vs[i] = Matrix4x4.TRS(T,Q,S).MultiplyPoint3x4(MF.mesh.vertices[i]);

		MF.mesh.vertices = Vs;
	}

	// DECOMPOSE AND SEND TO TRANSFORM TRS
	// public static void SetMatrix(this MeshFilter MF, Matrix4x4 M)
	// {		
	// 	Vector3[] Vs = new Vector3[M.vertices.Length];

	// 	for (int i = 0; i<M.vertices.Length; i++)
	// 		Vs[i]=M.inverse.MultiplyPoint3x4(M.vertices[i]);
	// 	M.vertices = Vs;

	// 	M.transform.SetMatrix(M);
	// }

	// public static void SetMatrix(this Transform T, Matrix4x4 M)
	// {
	// 	T.localPosition = M.GetColumn(3);
	// 	//T.localRotation = Quaternion.LookRotation( M.GetColumn(2),M.GetColumn(1) );

	// 	T.localRotation = M.AsQuaternion();

	// 	T.localScale = new Vector3( M.GetColumn(0).magnitude, M.GetColumn(1).magnitude, M.GetColumn(2).magnitude );
	// }

	// public static void ApplyMatrix(this Transform T, Matrix4x4 M)
	// {
	// 	M = M.inverse;
	// 	T.localPosition += (Vector3)M.GetColumn(3);
	// 	T.localScale = Vector3.Scale(T.localScale, new Vector3( M.GetColumn(0).magnitude, M.GetColumn(1).magnitude, M.GetColumn(2).magnitude));
	// 	T.localRotation = Quaternion.LookRotation( M.GetColumn(2),M.GetColumn(1) ) * T.localRotation;
	// 	//T.localRotation = T.localRotation * M.AsQuaternion();
	// }

	// public static Quaternion AsQuaternion(this Matrix4x4 m) 
	// {
	// 	Quaternion q = new Quaternion();
	// 	q.w = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] + m[1,1] + m[2,2] ) ) / 2; 
	// 	q.x = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] - m[1,1] - m[2,2] ) ) / 2; 
	// 	q.y = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] + m[1,1] - m[2,2] ) ) / 2; 
	// 	q.z = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] - m[1,1] + m[2,2] ) ) / 2; 
	// 	q.x *= Mathf.Sign( q.x * ( m[2,1] - m[1,2] ) );
	// 	q.y *= Mathf.Sign( q.y * ( m[0,2] - m[2,0] ) );
	// 	q.z *= Mathf.Sign( q.z * ( m[1,0] - m[0,1] ) );
	// 	return q;
	// }
}