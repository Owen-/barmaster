﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class DebugTools : MonoBehaviour
{
	private static DebugTools instance;
	private static bool shouldDebug = false;
	private int menuIndex = 0;

	private System.Type[] allTypes;
	private System.Type[] AllTypes 
	{ 
		get 
		{ 
			if (allTypes == null) allTypes = GetAllTypes();
			return allTypes;
		}
	}

	[MenuItem ("Barmaster/Toggle Debugging %g", true)]
	public static bool ValidateToggleDebugging()
	{
		return Application.isPlaying; // replace with is in play mode
	}

	[MenuItem ("Barmaster/Toggle Debugging %g")]
	public static void ToggleDebuging(MenuCommand MC)
	{
		if (instance == null) instance = new GameObject("Debugger").AddComponent<DebugTools>();
		shouldDebug=!shouldDebug;
	}

	// you can instance twice?
	private void OnGUI()
	{
		if (shouldDebug)
		{
			GUILayout.BeginArea(new Rect(0,0,Screen.width/2f, Screen.height), "Debugging Tools");

				GUILayout.Label("Time Scale");
				Time.timeScale 	= GUILayout.HorizontalSlider	(Time.timeScale, 0.0F, 1.0F);
				menuIndex 		= GUILayout.Toolbar 			(menuIndex,  new string[] {"Type Count", "Object Information", "About"});

				switch (menuIndex)
				{
					case(0):
						// GUILayout.Label("Object Count = " + AllTypes.Length); break;
						GUILayout.Label("GameObject Count = " + GameObject.FindObjectsOfType<GameObject>().Length); 
						foreach (System.Type T in AllTypes)
						{
							GUILayout.Label("GameObject Count = " + GameObject.FindObjectsOfType<GameObject>().Length); 
						}
						break;
					case(1):
						GUILayout.Label("Someinfo"); break;
					case(2):
						GUILayout.Label("Possibly the best debugging toolset ever made, brilliantly crafted by a genius"); break;
				}
				

			GUILayout.EndArea();
		}
	}

	public System.Type[] GetAllDerivedTypes(System.Type aType)
	{
		var result = new List<System.Type>();
		var assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
		foreach (var assembly in assemblies)
		{
			var types = assembly.GetTypes();
			foreach (var type in types)
			{
				if (type.IsSubclassOf(aType))
			    	result.Add(type);
			}
		}
		return result.ToArray();
	}

	public System.Type[] GetAllTypes()
	{
		var result = new List<System.Type>();
		var assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
		foreach (var assembly in assemblies)
		{
			var types = assembly.GetTypes();
			foreach (var type in types)
			    	result.Add(type);
		}
		return result.ToArray();
	}
}
