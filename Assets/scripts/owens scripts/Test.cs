﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour 
{
	public MeshFilter meshF;

	public bool SetPiv = false;
	//public bool glob;

	public Vector3 t;
	public Vector3 r;
	public Vector3 s;

	private void Update()
	{
		if (SetPiv) setPiv();
	}

	private void setPiv()
	{
		meshF.TransformTRS(t,r,s);
		SetPiv=false;
	}
}
