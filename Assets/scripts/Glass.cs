using UnityEngine;
using System.Collections;

public class Glass : MonoBehaviour
{
	public  Transform spawnPoint;
	private Transform model;

	public Flightpath flightpath;
	[HideInInspector] public Flightpath fp;

	[HideInInspector] public float height;
	private float pivot = 0;

	private void Awake()
	{
		model = transform.GetChild(0);
		height = model.localScale.y;

		// transform.position = spawnPoint.position;
		// transform.rotation = spawnPoint.rotation;

		fp = Instantiate(flightpath) as Flightpath;
	}

	public void setPivot(float pivotDistance)
	{
		pivot = -height*pivotDistance;
		model.localPosition = new Vector3(0,pivot,0);
	}

	public void lerpPivot(float t)
	{
		model.localPosition = new Vector3(0, Mathf.Lerp(pivot,0,t), 0);
	}
}

