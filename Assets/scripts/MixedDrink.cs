using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class MixedDrink : Drink
{
	private List<DrinkType> possibleTypes;
	private List<Glass>     possibleGlassTypes;    // change to arrays ???
	private List<Glass>     glasses;

	private bool isInvalid     = false;
	private bool choosingGlass = false;

	protected override void Awake()
	{
		base.Awake();

		possibleTypes = Overlord.instance.orderManager.level.drinks.Select( o => o.d ).ToList();
		glasses = new List<Glass>();
	}

	public int findPossibleTypes(Hand hand)
	{
		possibleTypes = possibleTypes.Where( o => o.recipe.ContainsKey(hand.member.ingredient) ).ToList();  

		if (possibleTypes.Count == 1) 
		{
			setType(possibleTypes[0]);
			glass = summonGlass(type.glass, hand.focus, 0.5f);
			Debug.Log("GLASS SUMMONED");
		}

		return possibleTypes.Count;
	}

	public bool findPossibleGlasses(Hand hand)
	{
		possibleGlassTypes = possibleTypes.Select( o => o.glass ).Distinct().ToList();

		// if      (possibleTypes.Count == 0) destroyDrink(hand);              //remove? 
		// else if (possibleTypes.Count == 1) setType(possibleTypes[0]);       //remove?

		if (possibleGlassTypes.Count == 1) 
		{
			glass = summonGlass(possibleGlassTypes[0],hand.focus,0.5f);
			return true;
		}
		else
		{
			Debug.Log("GLASS UNCERTAIN " + possibleGlassTypes.Count);
			foreach(Glass g in possibleGlassTypes) glasses.Add(summonGlass(g,hand.focus,0.5f)); // change hand.focus to a subfocus
			return false;
		}
	}

	public override void findTypeFinal()	
	{
		foreach (DrinkType t in possibleTypes) 
			if (this.recipeCheck(t)) 
				{ setType(t); break; }
	}


	public void chooseGlass(Hand hand)
	{
		Debug.Log("CHOSEN");
		// int i = 0;
		int i = (int)Mathf.Floor(hand.a/360f * glasses.Count);

		glass = glasses[i];
		possibleTypes = possibleTypes.Where(o => o.glass==possibleGlassTypes[i]).ToList();

		if (possibleTypes.Count == 1) setType(possibleTypes[0]);

		for (int j=0; j<glasses.Count; j++)
			if (j!=i) Destroy(glasses[j]);    // make them fly back rather than deleting

		glasses = null;

		// move glass to focus
		choosingGlass = false;
	}
}