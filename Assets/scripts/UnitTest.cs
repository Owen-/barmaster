using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class UnitTest : MonoBehaviour 
{
	public DrinkType a, b, c, d;

	public Ingredient e, f, g, h;

	private void Start()
	{
		// listTest();

		//dictionaryTest();
		
	}

	public void dictionaryTest()
	{
		Dictionary<Ingredient,int> A = new Dictionary<Ingredient,int>()
		{
			{e, 4}, {f, 8}, {g, 4}
		};

		Dictionary<Ingredient, int> B = new Dictionary<Ingredient, int>()
		{
			{e, 5}, {f, 9}, {g, 5}
		};

		Debug.Log(A);  // i put these in just so it would stop complaining about unused variables
		Debug.Log(B);
	}

	public void listTest()
	{
		// int[] C = { 1, 2, 3, 4, 5 };
		// int[] D = { 1, 2, 3, 3 };

		// DrinkType[] A = { a, b, c, d };
		// DrinkType[] B = { c, a, d, b };

		// Debug.Log(ListFunctions.scrambledEquals<DrinkType>(A,B));

		// DrinkType[] C = { a, b, c, d, d, d };
		// DrinkType[] D = { a, b, c, c };

		// Debug.Log(D.isSubsetOf<DrinkType>(C));


		// Drink p = new Drink(),
		//       q = new Drink(),
		//       r = new Drink(),
		//       s = new Drink();

		// Drink[]       E = { p, q, r, s };
		// DrinkType[]  F = { a, b, c, d, a };

		// Debug.Log(F.isSubsetOf<DrinkType,Drink>(E));
	}
	
}