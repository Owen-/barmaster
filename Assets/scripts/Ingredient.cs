using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName="Barmaster/Ingredient")]
[System.Serializable]
public class Ingredient : ScriptableObject
{
	public Color color;
	public Material material;
	
	[System.Serializable]
	public class IngredientWithQuantity
	{
		public Ingredient i;
		public int        q = 1;
	}
}


