using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(Animator))]
public abstract class Section : MonoBehaviour
{
	protected List<SectionMember> members;

	protected Animator animator;

	private void Awake()
	{
		members  = new List<SectionMember>();
	}

	private IEnumerator Start()
	{
		animator = GetComponent<Animator>();
		
		yield return null;
		members = members.OrderBy(o=>o.transform.position.x).ToList();
	}

	public void addMember(SectionMember member) { members.Add(member); }						

	public virtual void highlight ()  { animator.SetBool("highlighted", true); }
	public virtual void unhighlight() { animator.SetBool("highlighted", false); }

	protected virtual void deadzoneAction(Hand hand)
	{
		//triggerAction(); //this doesn't exist for sections yet, but may be useful
		// foley.audioClick(); play some kind of error noise

		hand.setState(Overlord.instance.sectionManager.choosingSection);
	}

	public virtual void choosingMember(Hand hand)
	{
		if (hand.r < Hand.deadzone)
		{
			if (hand.member != null) { hand.member.unhighlight(); hand.member = null; }

			if (hand.A) deadzoneAction(hand);
		}
		else
		{
			int i = (int)Mathf.Floor( (hand.x+1f)*(float)members.Count/2f );   
			if (i==members.Count) i--; 

			hand.setMember(members[i]);

			if (hand.A) hand.setState(hand.section.useSelectedMember);
		}
	}

	public virtual void useSelectedMember(Hand hand) 
	{
		Debug.Log("called virtual function useSelectedMember");
	}
}


