using UnityEngine;
using System.Collections;

public class WineSection : Section 
{
	public override void useSelectedMember(Hand hand)
	{
		hand.member.setUp(hand);

		hand.setState(flyingAndPouring);
	}

	private void flyingAndPouring(Hand hand)
	{
		hand.member.selfManage(hand);

		if (hand.A)	completeDrink(hand);
	}


	private void completeDrink(Hand hand)
	{
		hand.member.shutDown();

		hand.drink.evaluate(hand);

		hand.setState(Overlord.instance.sectionManager.choosingSection);
	}
}
