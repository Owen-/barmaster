using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

public class SpiritSection : Section  
{
	// WoooOOOooOOoooooooOOOo

	protected override void deadzoneAction(Hand hand)
	{
		if (hand.drink == null) base.deadzoneAction(hand);
		else
		{
			Assert.IsNotNull(hand.drink.type); // remove later

			if (hand.drink.type == null) hand.drink.findTypeFinal();
			
			hand.drink.evaluate(hand);
			Overlord.instance.orderManager.addMadeDrink(hand);
		}
	}

	public override void useSelectedMember(Hand hand)
	{
		hand.member.setUp(hand);

		if (hand.drink==null)           // if drink does not exist yet
		{
			hand.drink = Instantiate(Overlord.instance.mixedDrinkPrefab, hand.focus, false) as Drink;
			
			int  n = (hand.drink as MixedDrink).findPossibleTypes(hand);   // n is not used
			bool b = (hand.drink as MixedDrink).findPossibleGlasses(hand);

			if (b) hand.setState(hand.section.choosingMember);
			else   hand.setState(choosingGlass);
		}
		else if (hand.drink.type!=null) // if drink exists with known type
		{
			if (!hand.drink.type.recipe.ContainsKey(hand.member.ingredient)) destroyDrink(hand);
			else hand.setState(hand.section.choosingMember);
		}
		else                            // if drink exists with unknown type
		{
			int n = (hand.drink as MixedDrink).findPossibleTypes(hand);
			if (n == 0) destroyDrink(hand);
		}

		hand.drink.addIngredient(hand.member.ingredient, 1f);
	}

	private void destroyDrink(Hand hand)
	{
		Debug.Log("DESTROY DRINK SMASH"); 
		hand.drink = null;
		hand.setState(Overlord.instance.sectionManager.choosingSection);
	}

	private void choosingGlass(Hand hand)
	{
		Debug.Log("CHOOSING GLASS");

		if (hand.A) 
		{
			(hand.drink as MixedDrink).chooseGlass(hand);
			hand.setState(hand.section.choosingMember);
		}
	}
}


// always choose glass after first ingredient