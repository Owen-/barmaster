using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class Drink : MonoBehaviour
{
	public  Dictionary<Ingredient, float> ingredients;

	public  DrinkType type; // reference to the type of drink this is
	public  Glass glass;
	private Flightpath fp;

	public bool isGood = false; // shows whether the the drink was made right or not

	private Transform fluidShape;
	private Transform headShape;
	private const float fluidRadius = 0.03f;

	protected Animator animator;
	protected AnimatorStateInfo state;

	protected Foley foley;

	protected virtual void Awake()
	{
		ingredients = new Dictionary<Ingredient, float>();
		animator = GetComponent<Animator>();
		foley    = GetComponent<Foley>();
	}

	private void FixedUpdate()
	{
		state = animator.GetCurrentAnimatorStateInfo(0);

		if (state.IsName("base.glass flying to centre"))
		{
			Debug.Log("glass flying to centre");

			flyGlasses();
		}
		else if (state.IsName("base.completing drink"))
		{
			// Debug.Log("completing drink"); 
			fp.evaluate(transform, state.normalizedTime);
			glass.lerpPivot(state.normalizedTime);
		}
		else if (state.IsName("base.glass choice"))
		{
			Debug.Log("glass choice"); 
		}
	}

	protected virtual void flyGlasses()
	{
		glass.fp.evaluate(glass.transform, state.normalizedTime);
	}

	public void setType(DrinkType _type)
	{
		Debug.Log("set drink to type " + _type.ToString());
		type = _type;

		fp = Object.Instantiate(type.completionFlightpath) as Flightpath;

		// animator.SetTrigger("action");
	}

	public Glass summonGlass(Glass glassPrefab, Transform target, float pivot)
	{
		Glass g = Instantiate(glassPrefab, transform, false) as Glass;
		g.setPivot(pivot);
		g.fp.setTarget(g.transform, target);

		return g;
	}

	public void evaluate(Hand hand)
	{
		isGood = recipeCheck(type);

		if (isGood)
		{
			Overlord.instance.orderManager.addMadeDrink(hand);
			fp.setTarget(transform, Overlord.instance.orderManager.nextMadeDrinkPosition, Quaternion.identity);
			// animator.SetTrigger("action");
		}
	}

	public void addIngredient(Ingredient i, float quantity)
	{
		if (ingredients.ContainsKey(i)) ingredients[i] += quantity;
		else                            ingredients.Add(i, quantity);
	}

	public bool recipeCheck(DrinkType _type)
	{
		var owned = ingredients.ToDictionary( o=>o.Key, o=>(int)Mathf.Round(o.Value) ); // convert floats to ints

		if (_type.recipe==null || owned==null) return false;
		if (_type.recipe.Count != owned.Count) return false;

		foreach (KeyValuePair<Ingredient,int> i in _type.recipe)
		{
			if ( owned.ContainsKey(i.Key) )
			{
				if (owned[i.Key] >= i.Value) owned.Remove(i.Key);  // change to need right amount
			} 
			else return false;
		}

		if ( owned.Count==0 ) return true;     // if all ingredients have been ticked off and there are no others..
		else                  return false;
	}

	public void pint_MakeCylinders(Material fluidMat, Material headMat)
	{
		fluidShape = GameObject.CreatePrimitive(PrimitiveType.Cylinder).transform; 
		headShape  = GameObject.CreatePrimitive(PrimitiveType.Cylinder).transform; 

		fluidShape.SetParent(transform);                    
		 headShape.SetParent(transform);
		fluidShape.localPosition = new Vector3(0f,-glass.height,0f);        
		 headShape.localPosition = new Vector3(0f,-glass.height,0f); 
		fluidShape.localScale    = new Vector3(fluidRadius*2f,0f,fluidRadius*2f); 
		 headShape.localScale    = new Vector3(fluidRadius*2f,0f,fluidRadius*2f);

		fluidShape.GetComponent<MeshRenderer>().material = fluidMat;
		 headShape.GetComponent<MeshRenderer>().material = headMat;
	}

	public void pint_UpdateCylinders(Ingredient fluid, Ingredient head)
	{
		// Debug.Log(hand.drink.ingredients[ingredient]);

		float l_fluid = ingredients[fluid]*1e-6f / (Mathf.PI*fluidRadius*fluidRadius),
		      l_head  = ingredients[head]*1e-6f  / (Mathf.PI*fluidRadius*fluidRadius);

		fluidShape.localScale    = new Vector3(fluidRadius*2f, 0.5f*l_fluid, fluidRadius*2f);
		 headShape.localScale    = new Vector3(fluidRadius*2f, 0.5f*l_head,  fluidRadius*2f);
		fluidShape.localPosition = new Vector3(0f,-glass.height+0.5f*l_fluid,0f); 
		 headShape.localPosition = new Vector3(0f,-glass.height+0.5f*l_head+l_fluid,0f); 
	}

	public virtual void update(Ingredient ingredient, Hand hand) { Debug.Log("virtual method called"); }

	protected virtual void invalidDrink(Hand hand)               { Debug.Log("virtual method called"); }

	public virtual void findTypeFinal()                          { Debug.Log("virtual method called"); }
}

