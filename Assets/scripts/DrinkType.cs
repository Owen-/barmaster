using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[CreateAssetMenu(menuName="Barmaster/DrinkType")]
public class DrinkType : ScriptableObject, IEquivalentable
{
	[System.Serializable]
	public class DrinkTypeWithProbability
	{
		public DrinkType d;
		public float p = 1;
	}

	public List<Ingredient.IngredientWithQuantity> _recipe;
	public Dictionary<Ingredient,int> recipe;   // make this read-only somehow

	// private List<Decoration> decorations;

	[Space(20)]

	public Glass  glass;
	public Sprite sprite;
	public Flightpath completionFlightpath;

	[Space(20)]

	public int value;       // unused variables
	public bool isOrdered;

	private void OnEnable()
	{
		recipe = Enumerable.Range(0, _recipe.Count)
		         .ToDictionary(i => _recipe[i].i, i => _recipe[i].q);
	}

	public bool isEquivalentTo<T>(T target)
	{
		Drink drink = target as Drink;

		if (drink.type == null) Debug.Log("can't equivolate drink that has type null.");

		if (drink.type==this && drink.isGood) return true;
		else                                  return false;
	}
}


