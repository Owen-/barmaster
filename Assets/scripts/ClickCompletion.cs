using UnityEngine;
using System.Collections;


public class ClickCompletion : MonoBehaviour 
{
	public Order order;

	public void complete()
	{
		if (order.isComplete) Overlord.instance.orderManager.sendOrder(order);
		Debug.Log("CLICK COMPLETION");
	}

	// private void OnMouseDown()

}